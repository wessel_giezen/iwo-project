#/bin/bash

echo

DATA13="zless /net/corpora/twitter2/Tweets/2017/01/201701*:13.out.gz"
DATA16="zless /net/corpora/twitter2/Tweets/2017/01/201701*:16.out.gz"
#The raw data between 13.00u-14.00u is represented as variable $DATA13
#The raw data between 16.00u-17.00u is represented as variable $DATA16
EXTRACTOR="/net/corpora/twitter2/tools/tweet2tab text"
#The program that extracts the text data from the tweets is represented
#as variable $EXTRACTOR
BORRELTERMEN="zoektermenborrel.txt"
KOFFIETERMEN="zoektermenkoffiemoment.txt"
#The words that are used to distinguish the two moments are stored in
#the .txt files

echo "Amount of koffiemoment tweets between 13.00u and 14.00u:"
$DATA13 | $EXTRACTOR | sort -u | grep -ciFf $KOFFIETERMEN
#The DATA13 is extracted by the EXTRACTOR, the double tweets are then
#removed and the tweets are then filtered by the strings in KOFFIETERMEN
#and are then counted
echo

echo "Amount of borrel tweets between 13.00u and 14.00u:"
$DATA13 | $EXTRACTOR | sort -u | grep -ciFf $BORRELTERMEN
#The DATA13 is extracted by the EXTRACTOR, the double tweets are then
#removed and the tweets are then filtered by the strings in BORRELTERMEN
#and are then counted
echo

echo "Amount of koffiemoment tweets between 16.00u and 17.00u:"
$DATA16 | $EXTRACTOR | sort -u | grep -ciFf $KOFFIETERMEN
#The DATA16 is extracted by the EXTRACTOR, the double tweets are then
#removed and the tweets are then filtered by the strings in KOFFIETERMEN
#and are then counted
echo

echo "Amount of borrel tweets between 16.00u and 17.00u:"
$DATA16 | $EXTRACTOR | sort -u | grep -ciFf $BORRELTERMEN
#The DATA16 is extracted by the EXTRACTOR, the double tweets are then
#removed and the tweets are then filtered by the strings in BORRELTERMEN
#and are then counted
echo

echo "Amount of unique tweets in sample between 13.00u and 14.00u:"
$DATA13 | $EXTRACTOR | sort -u | wc -l
#The DATA13 is extracted by the EXTRACTOR, the double tweets are then
#removed and the tweets are then counted
echo

echo "Amount of unique tweets in sample between 16.00u and 17.00u:"
$DATA16 | $EXTRACTOR | sort -u | wc -l
#The DATA16 is extracted by the EXTRACTOR, the double tweets are then
#removed and the tweets are then counted
echo
