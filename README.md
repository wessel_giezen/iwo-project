This git-repository serves as a place to store the scripts that I'll use for a project for Introduction to Research Methods at the University of Groningen.

The repository contains a shell-scipt I've used for the final project (final-project.sh) and .txt files containing words that indicated the "borrel" or "koffiemoment".

The script has to be run on a LWP at the University of Groningen to get the optimal result.